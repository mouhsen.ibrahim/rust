use learn::k8s;

#[tokio::main]
async fn main() {
    k8s().await;
}
