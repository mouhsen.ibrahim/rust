use std::io;

trait Finder {
    fn find(&self, path: &str, text: &str) -> Result<bool, io::Error>;
}

struct Folder {
    _name: String,
}

impl Finder for Folder {
    fn find(&self, _path: &str, _text: &str) -> Result<bool, io::Error> {
        Ok(true)
    }
}

fn main() {
    println!("Hello, world!");
}
