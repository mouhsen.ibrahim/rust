use k8s_openapi::api::core::v1::Secret;
use std::process::Command;
use std::str::{self, FromStr};

use crate::errors::{self, Error};
use crate::kubernetes::{create_minikube_cluster, Helm, HelmChart, HelmRelease, Helmer};
use kube::{config::KubeConfigOptions, Api, Client, Config};

pub fn create_grafana() -> Result<(), errors::Error<std::io::Error>> {
    create_minikube_cluster("1.30.0");
    let helm = Helm {};
    match helm.add_helm_repo("grafana", "https://grafana.github.io/helm-charts") {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    let grafana_release = HelmRelease {
        chart: HelmChart {
            name: "grafana",
            repo: "grafana",
        },
        name: "my-grafana",
        version: "7.3.9",
        timeout: "100s",
        ..Default::default()
    };
    match helm.install_helm_chart(grafana_release, "minikube") {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    Ok(())
}

pub async fn grafana_print_password(context: &str) -> Result<(), Error<kube::error::Error>> {
    let kube_config_options = KubeConfigOptions {
        context: Some(String::from_str(context).expect("msg")),
        ..Default::default()
    };
    let config = Config::from_kubeconfig(&kube_config_options).await?;
    let client = Client::try_from(config)?;

    let secrerts: Api<Secret> = Api::namespaced(client, "default");
    let secret = secrerts.get("my-grafana").await?;
    println!(
        "password: {:?}",
        str::from_utf8(&secret.data.expect("").get("admin-password").expect("msg").0).expect("msg")
    );
    Ok(())
}

pub async fn grafana_port_forward() -> Result<(), Error<kube::error::Error>> {
    println!("access grafana at this URL http://localhost:3000 with this password");
    grafana_print_password("minikube").await?;
    let _ = Command::new("kubectl")
        .arg("port-forward")
        .arg("service/my-grafana")
        .arg("-n")
        .arg("default")
        .arg("3000:80")
        .status()?;
    Ok(())
}
