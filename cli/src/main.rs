use azuredevops::{DevOps, DevOpsClient};
use clap::{Parser, Subcommand};
use cli::create_topic;
use grafana::{create_grafana, grafana_port_forward};
use kubernetes::{create_minikube_cluster, K8s, Kubernetes};
use resources::Resource;
use rust_service::{RustService, Service};
use std::{collections::BTreeMap, env};

use argocd::{argocd_create_app, argocd_port_forward, run_argocd};
use my_gitlab::{create_deploy_key, gitlab_print_password, run_gitlab, DeployKey, Gitlab};
use prometheus::{prometheus_port_forward, prometheus_run};
use ssh_key::PrivateKey;
use std::path::Path;
use tekton::{
    tekton_event_listener, tekton_event_port_forward, tekton_pipeline_create,
    tekton_pipeline_run_create, tekton_port_forward, tekton_run, tekton_task, tekton_task_run,
    tekton_trigger_binding, tekton_trigger_run, tekton_trigger_template, tekton_trigger_test,
};

mod argocd;
mod azuredevops;
mod errors;
mod grafana;
mod kubernetes;
mod my_gitlab;
mod prometheus;
mod resources;
mod rust_service;
mod tekton;
mod utils;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Topic {
        title: String,
        link: String,
    },
    Argocd {
        #[command(subcommand)]
        command: Option<ArgocdCommands>,
    },
    Prometheus {
        #[command(subcommand)]
        command: Option<PrometheusCommands>,
    },
    Tekton {
        #[command(subcommand)]
        command: Option<TektonCommands>,
    },
    Gitlab {
        #[command(subcommand)]
        command: Option<GitlabCommands>,
    },
    AzureDevops {
        #[command(subcommand)]
        command: Option<AzureDevopsCommands>,
    },
    Service {
        #[command(subcommand)]
        command: Option<ServiceCommands>,
    },
    Grafana {
        #[command(subcommand)]
        command: Option<GrafanaCommands>,
    },
    Testing {
        #[command(subcommand)]
        command: Option<TestingCommands>,
    },
}

#[derive(Subcommand)]
enum ArgocdCommands {
    Create {},
    PortForward {},
    CreateApp {
        #[arg(long)]
        repository_url: String,
        #[arg(long)]
        path: String,
    },
}

#[derive(Subcommand)]
enum PrometheusCommands {
    Create {},
    PortForward {},
}

#[derive(Subcommand)]
enum TektonCommands {
    Create {},
    TriggerRun {},
    TriggerTemplate {},
    TriggerBinding {},
    EventListener {},
    EventPortForward {},
    TriggerTest {
        #[arg(long)]
        name: String,
    },
    PortForward {},
    Task {},
    TaskRun {},
    Pipeline {},
    PipelineRun {},
}

#[derive(Subcommand)]
enum GitlabCommands {
    Create {},
    PrintPassword {},
}

#[derive(Subcommand)]
enum AzureDevopsCommands {
    List {},
    ListRuns {
        #[arg(long)]
        pipeline_id: i32,
    },
    SearchLogs {
        #[arg(long)]
        pipeline_id: i32,
        #[arg(long)]
        run_id: i32,
        #[arg(long)]
        text: Vec<String>,
    },
    SaveLogs {
        #[arg(long)]
        pipeline_id: i32,
        #[arg(long)]
        run_id: i32,
    },
}

#[derive(Subcommand)]
enum ServiceCommands {
    Build {
        #[arg(long)]
        tag: String,
    },
    Deploy {
        #[arg(long)]
        tag: String,
    },
}

#[derive(Subcommand)]
enum GrafanaCommands {
    Create {},
    PortForward {},
}

#[derive(Subcommand)]
enum TestingCommands {
    Test {},
    Test2 {},
}

#[tokio::main]
async fn main() {
    let cli = Cli::parse();

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level cmd
    match &cli.command {
        Some(Commands::Topic { title, link }) => {
            create_topic(title, link).await;
        }
        Some(Commands::Argocd { command }) => match command {
            Some(ArgocdCommands::Create {}) => run_argocd().expect("cannot install argocd"),
            Some(ArgocdCommands::PortForward {}) => {
                argocd_port_forward("minikube")
                    .await
                    .expect("cannot port forward argocd");
            }
            Some(ArgocdCommands::CreateApp {
                repository_url,
                path,
            }) => {
                argocd_create_app(repository_url.clone(), path.clone()).await;
            }
            &None => {}
        },
        Some(Commands::Prometheus { command }) => match command {
            Some(PrometheusCommands::Create {}) => {
                prometheus_run().expect("cannot install prometheus")
            }
            Some(PrometheusCommands::PortForward {}) => {
                prometheus_port_forward().expect("cannot port forward")
            }
            &None => {}
        },
        Some(Commands::Tekton { command }) => match command {
            Some(TektonCommands::Create {}) => tekton_run(),
            Some(TektonCommands::TriggerRun {}) => tekton_trigger_run(),
            Some(TektonCommands::TriggerTemplate {}) => tekton_trigger_template(),
            Some(TektonCommands::TriggerBinding {}) => tekton_trigger_binding(),
            Some(TektonCommands::EventListener {}) => tekton_event_listener().await,
            Some(TektonCommands::EventPortForward {}) => tekton_event_port_forward(),
            Some(TektonCommands::TriggerTest { name }) => tekton_trigger_test(name).await,
            Some(TektonCommands::PortForward {}) => tekton_port_forward(),
            Some(TektonCommands::Task {}) => tekton_task(),
            Some(TektonCommands::TaskRun {}) => tekton_task_run(),
            Some(TektonCommands::Pipeline {}) => tekton_pipeline_create(),
            Some(TektonCommands::PipelineRun {}) => tekton_pipeline_run_create(),
            &None => {}
        },
        Some(Commands::Gitlab { command }) => match command {
            Some(GitlabCommands::Create {}) => run_gitlab().expect("cannot install gitlab"),
            Some(GitlabCommands::PrintPassword {}) => gitlab_print_password("minikube")
                .await
                .expect("cannot print gitlab password"),
            None => {}
        },
        Some(Commands::AzureDevops { command }) => {
            let token = env::var("AZURE_DEVOPS_TOKEN").expect("msg");
            let org = env::var("AZURE_DEVOPS_ORG").expect("msg");
            let project = env::var("AZURE_DEVOPS_PROJECT").expect("msg");
            let c = DevOpsClient {
                base_url: "https://dev.azure.com",
                token: &token,
                org: &org,
                project: &project,
            };
            match command {
                Some(AzureDevopsCommands::List {}) => {
                    println!("{:?}", c.get_pipelines().await);
                }
                Some(AzureDevopsCommands::ListRuns { pipeline_id }) => {
                    println!("{:?}", c.get_runs(*pipeline_id).await);
                }
                Some(AzureDevopsCommands::SearchLogs {
                    pipeline_id,
                    run_id,
                    text,
                }) => {
                    if *run_id == 0 {
                        let runs = c.get_runs(*pipeline_id).await.expect("msg");
                        for run in runs.value {
                            println!("search logs for run: {}", run.id);
                            c.search_logs(*pipeline_id, run.id, text)
                                .await
                                .expect("msg");
                        }
                    } else {
                        c.search_logs(*pipeline_id, *run_id, text)
                            .await
                            .expect("msg");
                    }
                }
                Some(AzureDevopsCommands::SaveLogs {
                    pipeline_id,
                    run_id,
                }) => {
                    if *run_id == 0 {
                        let runs = c.get_runs(*pipeline_id).await.expect("msg");
                        for run in runs.value {
                            println!("save logs for run: {}", run.id);
                            c.save_logs(*pipeline_id, run.id).await.expect("msg");
                        }
                    } else {
                        c.save_logs(*pipeline_id, *run_id).await.expect("msg");
                    }
                }
                None => {}
            }
        }
        Some(Commands::Service { command }) => match command {
            Some(ServiceCommands::Build { tag }) => {
                let s = RustService {};
                s.build(tag).expect("cannot build service");
            }
            Some(ServiceCommands::Deploy { tag }) => {
                create_minikube_cluster("1.30.0");
                let s = RustService {};
                s.deploy(tag, "minikube".to_string()).await;
            }
            None => {}
        },
        Some(Commands::Grafana { command }) => match command {
            Some(GrafanaCommands::Create {}) => create_grafana().expect("cannot install grafana"),
            Some(GrafanaCommands::PortForward {}) => grafana_port_forward()
                .await
                .expect("cannot port forward grafana"),
            None => {}
        },
        Some(Commands::Testing { command }) => match command {
            Some(TestingCommands::Test {}) => {
                let private_key = PrivateKey::read_openssh_file(Path::new(".secrets/id_rsa"))
                    .expect("cannot read private key file");
                let public_key = private_key.public_key();
                let token = env::var("GITLAB_TOKEN").expect("msg");

                // Create the client.
                let client = gitlab::Gitlab::builder("gitlab.com", token)
                    .build_async()
                    .await
                    .expect("msg");

                let _gitlab = Gitlab { client };
                create_deploy_key(
                    DeployKey {
                        key: public_key.to_string(),
                        can_push: false,
                        title: "argocd-test".to_string(),
                        project: Some("mouhsen.ibrahim/argocd-manifests-test".to_string()),
                        ..Default::default()
                    },
                    _gitlab,
                )
                .await;
            }
            Some(TestingCommands::Test2 {}) => {
                create_minikube_cluster("1.30.0");
                let k8s = K8s {};
                let mut data = BTreeMap::new();
                data.insert("key".to_string(), "value".to_string());
                k8s.create_secret(
                    "test".to_string(),
                    "default".to_string(),
                    data,
                    "minikube".to_string(),
                )
                .await;
            }
            None => {}
        },
        None => {}
    }
}

struct K8SSecret<T> {
    name: String,
    namespace: String,
    _data: BTreeMap<T, T>,
}

impl Resource<String> for K8SSecret<String> {
    fn get_id(&self) -> Option<String> {
        Some(format!("{}/{}", self.name, self.namespace))
    }
    fn same_id(&self, other: &Self) -> bool {
        self.name == other.name && self.namespace == other.namespace
    }
}
