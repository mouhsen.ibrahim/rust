use k8s_openapi::api::core::v1::Namespace;

use kube::{api::ListParams, Api, Client};

#[tokio::main]
async fn main() -> Result<(), ()> {
    println!("Hello, world!");

    let client = Client::try_default()
        .await
        .expect("cannot create default k8s client");

    let namespaces: Api<Namespace> = Api::all(client);
    let namespaces = namespaces.list(&ListParams::default()).await.expect("msg");

    for ns in namespaces {
        println!("{}", ns.metadata.name.expect(""));
    }

    /*
        let pods: Api<Pod> = Api::namespaced(client, "kube-system");
        let p = pods.get("etcd-minikube").await.expect("msg");
        println!(
            "Got blog pod with containers: {:?}",
            p.spec.unwrap().containers
        );

        let client = Client::try_default().await.expect("msg");
        let secrets: Api<Secret> = Api::namespaced(client, "kube-system");

        let lp = ListParams::default();

        let p = secrets.list(&lp).await.expect("msg");
        println!("{:?}", p.items[0].metadata.name);
    */
    Ok(())
}
