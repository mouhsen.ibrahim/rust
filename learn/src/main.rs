// An attribute to hide warnings for unused code.
#![allow(dead_code, unused_variables)]

use std::env;
use std::io::Write;
use std::{
    collections::HashMap,
    fmt::Display,
    fs::{read_to_string, File},
    io, process,
    str::FromStr,
};

use gitlab::api::issues::{self};
use gitlab::api::{self, projects, AsyncQuery};
use gitlab::Gitlab;
use serde::Deserialize;

use learn::{Gender, JsonFormatter, Person, YamlFormatter};

pub fn error_test1() {
    let mut v = [String::from("a"), String::from("b")];
    let x = &v[0]; // let x = v[0]; is wrong because the index operator must return a reference to an owned value
    println!("{x}");
    v[0] = String::from("e");
    //println!("{x}"); // The value of v was borrowed by x 3 lines above which means after we change v one line above we can no longer use x
}

pub fn error_test2() {
    let x = String::from("x");
    let y = x; // Here the value is moved so we cannot use x after this
               //x.clear();
    println!("{y}");
    //println!("{x}"); // x is no longer usable because it was moved to y 3 lines above
}

fn print_refs(x: &i32, y: &i32) {
    println!("x is {x} and y is {y}");
}

fn failed_borrow() {
    let _x = 12;

    let y: &i32 = &_x;
    //let y: &'a i32 = &_x;
    // Attempting to use the lifetime `'a` as an explicit type annotation
    // inside the function will fail because the lifetime of `&_x` is shorter
    // than that of function failed_borrow.
    // A short lifetime cannot be coerced into a longer one.
}

fn explicit_life_times() {
    let (four, nine) = (4, 9);
    print_refs(&four, &nine);
    failed_borrow();
}

fn print_one(x: &i32) {
    println!("x is {x}");
}

fn add_one(x: &mut i32) {
    *x += 1;
}

fn print_multi(x: &i32, y: &i32) {
    println!("x is {x}, y is {y}");
}

fn pass_x<'a>(x: &'a i32, y: &i32) -> &'a i32 {
    x
}

fn functions() {
    let (x, y) = (7, 9);
    print_one(&x);

    print_multi(&x, &y);
    let mut z = &1;
    println!("z is {z}");
    {
        let u = 1;
        z = pass_x(&y, &u);
        // z = pass_x(&u, &y);
        // this doesn't work because u doesn't live as much as z
    }
    println!("z is {z}");

    let mut t = 10;
    add_one(&mut t);
}

struct Owner(i32);

impl Owner {
    fn add_one(&mut self) {
        self.0 += 1;
    }
    fn print(&self) {
        println!("0 is {}", self.0);
    }
}

fn methods() {
    let mut owner = Owner(12);
    owner.add_one();
    owner.print();
}

#[derive(Debug)]
struct Borrowed<'a>(&'a i32);

#[derive(Debug)]
struct NamedBorrowed<'a> {
    x: &'a i32,
    y: &'a i32,
}

#[derive(Debug)]
enum Either<'a> {
    Num(i32),
    Ref(&'a i32),
}

fn structs() {
    let x = 18;
    let y = 12;

    let single = Borrowed(&x);
    let double = NamedBorrowed { x: &x, y: &y };
    let number = Either::Num(x);
    let reference = Either::Ref(&x);

    println!("single is {:?}", single);
    println!("double is {:?}", double);
    println!("number is {:?}", number);
    println!("reference is {:?}", reference);
}

impl Default for Borrowed<'_> {
    fn default() -> Self {
        Self(&0)
    }
}

fn traits() {
    let b = Borrowed::default();

    println!("b is {:?}", b);
}

pub fn life_times() {
    println!("life times");
    let i = 3;

    {
        let b1 = &i;
        println!("b1 is {b1}");
    }

    {
        let b2 = &i;
        println!("b2 is {b2}");
    }
    println!("i is {i}");
    explicit_life_times();

    functions();
    methods();
    structs();
    traits();
}

#[derive(Debug)]
struct A;

#[derive(Debug)]
struct Single(A);

#[derive(Debug)]
struct SingleGen<T>(T);

fn generic_structs() {
    let _s = Single(A);
    let _char1: SingleGen<char> = SingleGen('a');

    let _t = SingleGen(A);
    let _i32 = SingleGen(32);
    let _char = SingleGen('a');

    println!(
        "_s is {:?}, _char1 is {:?}, _t is {:?}, _i32 is {:?}, _char is {:?}",
        _s, _char1, _t, _i32, _char
    );
}

fn reg_fn(_s: Single) {}

fn gen_spec_t(_s: SingleGen<A>) {}

fn gen_spec_i32(_s: SingleGen<i32>) {}

fn generic<T>(_s: SingleGen<T>) {}

fn generic_functions() {
    reg_fn(Single(A));
    gen_spec_t(SingleGen(A));
    gen_spec_i32(SingleGen(12));

    generic::<char>(SingleGen('a'));

    generic(SingleGen(32));
}

impl<T> SingleGen<T> {
    fn get(&self) -> &Self {
        self
    }
}

fn generic_implementaions() {}

fn printer<T: Display>(t: T) {
    println!("t is {}", t);
}

fn generic_bounds() {
    let x = String::from("mouhsen");
    printer(x);
}

pub fn generics() {
    generic_structs();
    generic_functions();
    generic_implementaions();
    generic_bounds();
}

pub fn parse<T>(s: &str) -> T
where
    T: std::str::FromStr,
    <T as FromStr>::Err: core::fmt::Debug,
{
    let s = s.trim();
    s.parse::<T>().expect("msg")
}

pub fn int_type() {
    let mut buffer = String::new();
    println!("please input a valid integer");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let i = parse::<i32>(&buffer);
    let buffer = buffer.trim();
    println!("read {i} from stdin");
}

pub fn float_type() {
    let mut buffer = String::new();
    println!("please input a valid float");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let f = parse::<f64>(&buffer);
    let buffer = buffer.trim();
    println!("read {f} from stdin");
}

pub fn str_type() {
    let mut buffer = String::new();
    println!("please input a valid string");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let buffer = buffer.trim();
    println!("read {buffer} from stdin");
}

pub fn array_type() {
    let mut buffer = String::new();
    let mut arr: [i32; 2] = [1, 2];
    println!("please input a valid integer as first element in the array");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let buffer = buffer.trim();
    let i = buffer.parse::<i32>().expect("cannot convert input to int");
    arr[0] = i;
    let mut buffer = String::new();
    println!("please input a valid integer as second element in the array");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let buffer = buffer.trim();
    let i = buffer.parse::<i32>().expect("cannot convert input to int");
    arr[1] = i;
    println!("read {} and {} from stdin", arr[0], arr[1]);
}

pub fn vector_type() {
    let mut buffer = String::new();
    println!("input number of vector elements");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let buffer = buffer.trim();
    let count: i32 = buffer.parse().expect("cannot convert input to int");
    let mut v: Vec<i32> = Vec::new();
    for i in 0..count {
        let mut buffer = String::new();
        println!("input element number {i}");
        io::stdin()
            .read_line(&mut buffer)
            .expect("cannot read from stdin");
        let buffer = buffer.trim();
        let el: i32 = buffer.parse().expect("cannot convert input to int");
        v.push(el);
    }
    println!("got {:?}", v);
}

pub fn insert_to_map<'a>(m: &mut HashMap<&'a str, &'a str>, key: &'a str, value: &'a str) {
    m.insert(key, value);
}

pub fn map_type() {
    println!("input key value pairs for the map, use end as key to exit");
    println!("input key now");
    let mut m: HashMap<String, String> = HashMap::new();
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("cannot read input");
    buffer = String::from(buffer.trim());
    let mut buffer_v;
    loop {
        buffer_v = String::new();
        println!("input value now");
        io::stdin()
            .read_line(&mut buffer_v)
            .expect("cannot read input");
        m.insert(buffer, buffer_v);
        buffer = String::new();
        io::stdin()
            .read_line(&mut buffer)
            .expect("cannot read input");
        let buffer = buffer.trim();
        if buffer == "end" {
            break;
        }
    }
    println!("you input {:?}", m);
}

fn read_lines(filename: &str) -> Vec<String> {
    read_to_string(filename)
        .unwrap() // panic on possible file-reading errors
        .lines() // split the string into an iterator of string slices
        .map(String::from) // make each slice into a string
        .collect() // gather them together into a vector
}

pub fn struct_type() {
    let lines = read_lines("data.txt");
    if lines.len() != 3 {
        println!("number of lines in file must be 3 exactly");
        process::exit(2);
    }
    let age: i8 = lines[0].parse().expect("cannot convert first line to int");
    let ge = lines[1].trim();
    let g: Gender;
    if ge == "m" || ge == "M" {
        g = Gender::Male
    } else if ge == "f" || ge == "F" {
        g = Gender::Female
    } else {
        println!("unkown gender {}", ge);
        process::exit(2);
    }
    let name = &lines[2];
    let p = Person {
        age,
        gender: g,
        name: name.to_string(),
    };
    println!("you input {:?}", p);
    let mut out = File::create("out.txt").expect("cannot ceate file");
    write!(out, "{}", p.introduce()).expect("cannot write to file");
}

pub fn trait_json() {
    let person = Person {
        age: 17,
        gender: Gender::Male,
        name: String::from_str("david").expect("msg"),
    };
    let js = person.format().expect("msg");
    let mut out = File::create("out.json").expect("cannot create file");
    write!(out, "{js}").expect("cannot write to a file");
}

pub fn trait_yaml() {
    let person = Person {
        age: 21,
        gender: Gender::Female,
        name: String::from_str("sarah").expect("msg"),
    };
    let js = person.format_yaml().expect("msg");
    let mut out = File::create("out.yaml").expect("cannot create file");
    write!(out, "{js}").expect("cannot write to a file");
}

pub fn types() {
    let mut buffer = String::new();
    println!("select type: int, float, string, array, vector, map, struct, trait_json, trait_yaml");
    io::stdin()
        .read_line(&mut buffer)
        .expect("error reading from stdin");
    let buffer = buffer.trim();
    match buffer {
        "int" => int_type(),
        "float" => float_type(),
        "string" => str_type(),
        "array" => array_type(),
        "vector" => vector_type(),
        "map" => map_type(),
        "struct" => struct_type(),
        "trait_json" => trait_json(),
        "trait_yaml" => trait_yaml(),
        _ => {
            println!("wrong choice");
            process::exit(1);
        }
    }
}

// The return type of a `Project`. Note that GitLab may contain more information, but you can
// define your structure to only fetch what is needed.
#[derive(Debug, Deserialize)]
struct Project {
    name: String,
}

#[derive(Debug, Deserialize)]
struct Issue {
    title: String,
}

pub async fn gitlab() {
    let token = env::var("GITLAB_TOKEN").expect("msg");

    // Create the client.
    let client = Gitlab::builder("gitlab.com", token)
        .build_async()
        .await
        .expect("msg");

    // Create a simple endpoint. This one gets the "gitlab-org/gitlab" project information.
    let endpoint = projects::Project::builder()
        .project("mouhsen.ibrahim/learning")
        .build()
        .unwrap();
    // Call the endpoint. The return type decides how to represent the value.
    let project: Project = endpoint.query_async(&client).await.unwrap();
    println!("project: {:?}", project);

    let issues_endpoint = issues::ProjectIssues::builder()
        .project("mouhsen.ibrahim/learning")
        .build()
        .expect("msg");
    let issues: Vec<Issue> = api::paged(issues_endpoint, api::Pagination::All)
        .query_async(&client)
        .await
        .expect("msg");
    for issue in issues {
        println!("issue: {}", issue.title);
    }
    /*let issues_endpoint = projects::issues::CreateIssue::builder()
        .project("mouhsen.ibrahim/testing")
        .title("new issue")
        .description("new description")
        .label("idea:new")
        .build()
        .expect("msg");
    let issue: Issue = issues_endpoint.query_async(&client).await.expect("msg");
    println!("Issue created is {:?}", issue)*/
}

#[tokio::main]
async fn main() {
    //error_test1();
    //error_test2();
    //life_times();
    //generics();
    //types();
    gitlab().await;
}
