use clap::{Parser, Subcommand};

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Testing {
        #[command(subcommand)]
        command: Option<TestingCommands>,
    },
}

#[derive(Subcommand)]
enum TestingCommands {
    Test1 {},
    Test2 {},
}

#[tokio::main]
async fn main() {
    let cli = Cli::parse();

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level cmd
    match &cli.command {
        Some(Commands::Testing { command }) => match command {
            Some(TestingCommands::Test1 {}) => {
                println!("test command 1");
            }
            Some(TestingCommands::Test2 {}) => {
                println!("test command 2");
            }
            None => {}
        },
        None => {}
    }
}
