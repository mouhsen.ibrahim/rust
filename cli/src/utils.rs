use ssh_key::{rand_core::OsRng, Algorithm, Error, PrivateKey};
use std::path::Path;

pub fn create_ssh_key_pair(path_pub: &String, path_priv: &String) -> Result<PrivateKey, Error> {
    let private_key = PrivateKey::random(&mut OsRng, Algorithm::Rsa { hash: None })?;
    let public_key = private_key.public_key();
    private_key.write_openssh_file(Path::new(path_priv), ssh_key::LineEnding::CR)?;
    public_key.write_openssh_file(Path::new(path_pub))?;
    Ok(private_key)
}
