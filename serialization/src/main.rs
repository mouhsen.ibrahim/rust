use std::fs;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Point {
    x: f64,
    y: f64,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Default)]
struct Config {
    projects: Vec<Project>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Project {
    name: String,
    id: String,
}

impl Config {
    fn read(&mut self, name: &str) {
        let c = fs::read_to_string(name).expect("msg");
        *self = serde_yaml::from_str(&c).expect("msg");
    }
}

fn main() -> Result<(), serde_yaml::Error> {
    let point = Point { x: 1.0, y: 2.0 };

    let yaml = serde_yaml::to_string(&point)?;
    println!("{yaml}");

    let deserialized_point: Point = serde_yaml::from_str(&yaml)?;
    println!("{:?}", deserialized_point);
    let mut c = Config::default();
    c.read("test.yaml");
    println!("{:?}", c);
    Ok(())
}
