use k8s_openapi::api::core::v1::{Secret, ServiceAccount};
use k8s_openapi::api::rbac::v1::{ClusterRoleBinding, RoleBinding, RoleRef, Subject};
use kube::api::{ObjectMeta, PostParams};
use kube::{config::KubeConfigOptions, Api, Client, Config};
use serde::Deserialize;
use std::collections::BTreeMap;
use std::io::Write;
use std::process::{Command, ExitStatus};
use std::str::{self, FromStr};
use tempfile::NamedTempFile;

pub trait KubernetesCluster {
    fn create_cluster(&self, name: &str, version: &str, memory: &str, cpu: &str);
    fn delete_cluster(&self, name: &str);
    fn get_auth(&self, name: String) -> String;
}

pub trait Kubernetes {
    fn apply<T>(&self, manifest: T)
    where
        T: Sized + serde::Serialize;
    async fn create_sa(&self, name: String, namespace: String, context: String);
    async fn create_role_binding(
        &self,
        name: String,
        namespace: String,
        context: String,
        sa_name: String,
        cluster_role_name: String,
    );
    async fn create_cluster_role_binding(
        &self,
        name: String,
        context: String,
        sa_name: String,
        cluster_role_name: String,
    );
    async fn create_secret(
        &self,
        name: String,
        namespace: String,
        data: BTreeMap<String, String>,
        context: String,
    );
}

pub struct K8s {}

impl Kubernetes for K8s {
    fn apply<T>(&self, manifest: T)
    where
        T: Sized + serde::Serialize,
    {
        let manifest = serde_yaml::to_string(&manifest).expect("msg");
        let mut tmpfile = NamedTempFile::new().unwrap();
        write!(tmpfile, "{manifest}").unwrap();
        let _ = Command::new("kubectl")
            .arg("apply")
            .arg("-f")
            .arg(tmpfile.path())
            .status()
            .expect("error applying k8s manifest");
    }
    async fn create_sa(&self, name: String, namespace: String, context: String) {
        let kube_config_options = KubeConfigOptions {
            context: Some(context),
            ..Default::default()
        };
        let config = Config::from_kubeconfig(&kube_config_options)
            .await
            .expect("msg");
        let client = Client::try_from(config).expect("cannot create default k8s client");

        let service_accounts: Api<ServiceAccount> = Api::namespaced(client.clone(), &namespace);

        let sa = ServiceAccount {
            metadata: ObjectMeta {
                name: Some(name.clone()),
                namespace: Some(namespace.clone()),
                ..Default::default()
            },
            automount_service_account_token: Some(true),
            ..Default::default()
        };

        service_accounts
            .create(&PostParams::default(), &sa)
            .await
            .expect("cannot create sa");
        let secrets: Api<Secret> = Api::namespaced(client, &namespace);

        let mut annotations = BTreeMap::new();
        annotations.insert(
            "kubernetes.io/service-account.name".to_string(),
            name.clone(),
        );
        annotations.insert(
            "kubernetes.io/service-account.namespace".to_string(),
            namespace.clone(),
        );

        let secret = Secret {
            metadata: ObjectMeta {
                name: Some(name),
                namespace: Some(namespace),
                annotations: Some(annotations),
                ..Default::default()
            },
            type_: Some("kubernetes.io/service-account-token".to_string()),
            ..Default::default()
        };

        secrets
            .create(&PostParams::default(), &secret)
            .await
            .expect("cannot create secret");
    }
    async fn create_role_binding(
        &self,
        name: String,
        namespace: String,
        context: String,
        sa_name: String,
        cluster_role_name: String,
    ) {
        let kube_config_options = KubeConfigOptions {
            context: Some(context),
            ..Default::default()
        };
        let config = Config::from_kubeconfig(&kube_config_options)
            .await
            .expect("msg");
        let client = Client::try_from(config).expect("cannot create default k8s client");

        let role_bindings: Api<RoleBinding> = Api::namespaced(client, &namespace);

        let role_binding = RoleBinding {
            metadata: ObjectMeta {
                name: Some(name),
                namespace: Some(namespace),
                ..Default::default()
            },
            subjects: Some(vec![Subject {
                kind: "ServiceAccount".to_string(),
                name: sa_name,
                ..Default::default()
            }]),
            role_ref: RoleRef {
                api_group: "rbac.authorization.k8s.io".to_string(),
                kind: "ClusterRole".to_string(),
                name: cluster_role_name,
            },
        };

        role_bindings
            .create(&PostParams::default(), &role_binding)
            .await
            .expect("cannot create role binding");
    }
    async fn create_cluster_role_binding(
        &self,
        name: String,
        context: String,
        sa_name: String,
        cluster_role_name: String,
    ) {
        let kube_config_options = KubeConfigOptions {
            context: Some(context),
            ..Default::default()
        };
        let config = Config::from_kubeconfig(&kube_config_options)
            .await
            .expect("msg");
        let client = Client::try_from(config).expect("cannot create default k8s client");

        let cluster_role_bindings: Api<ClusterRoleBinding> = Api::all(client);

        let cluster_role_binding = ClusterRoleBinding {
            metadata: ObjectMeta {
                name: Some(name),
                ..Default::default()
            },
            subjects: Some(vec![Subject {
                kind: "ServiceAccount".to_string(),
                name: sa_name,
                namespace: Some("default".to_string()),
                ..Default::default()
            }]),
            role_ref: RoleRef {
                api_group: "rbac.authorization.k8s.io".to_string(),
                kind: "ClusterRole".to_string(),
                name: cluster_role_name,
            },
        };

        cluster_role_bindings
            .create(&PostParams::default(), &cluster_role_binding)
            .await
            .expect("cannot create role binding");
    }
    async fn create_secret(
        &self,
        name: String,
        namespace: String,
        data: BTreeMap<String, String>,
        context: String,
    ) {
        let kube_config_options = KubeConfigOptions {
            context: Some(context),
            ..Default::default()
        };
        let config = Config::from_kubeconfig(&kube_config_options)
            .await
            .expect("msg");
        let client = Client::try_from(config).expect("cannot create default k8s client");
        let secrets: Api<Secret> = Api::namespaced(client, &namespace);

        let mut labels = BTreeMap::new();
        labels.insert(
            "argocd.argoproj.io/secret-type".to_string(),
            "repository".to_string(),
        );

        let secret = Secret {
            metadata: ObjectMeta {
                name: Some(name),
                namespace: Some(namespace),
                labels: Some(labels),
                ..Default::default()
            },
            string_data: Some(data),
            ..Default::default()
        };

        secrets
            .create(&PostParams::default(), &secret)
            .await
            .expect("cannot create secret");
    }
}

#[derive(Default)]
pub struct Minikube<'a> {
    _addons: Vec<&'a str>,
}

pub trait Helmer {
    fn add_helm_repo(&self, name: &str, url: &str) -> Result<ExitStatus, std::io::Error>;
    fn install_helm_chart(
        &self,
        release: HelmRelease,
        context: &str,
    ) -> Result<ExitStatus, std::io::Error>;
}

pub struct Helm {}

#[derive(Default)]
pub struct HelmChart<'a> {
    pub name: &'a str,
    pub repo: &'a str,
}

#[derive(Default)]
pub struct HelmRelease<'a> {
    pub chart: HelmChart<'a>,
    pub name: &'a str,
    pub timeout: &'a str,
    pub values: Vec<HelmValue<'a>>,
    pub values_file: &'a str,
    pub version: &'a str,
}

pub struct HelmValue<'a> {
    pub key: &'a str,
    pub value: &'a str,
}

impl Helmer for Helm {
    fn install_helm_chart(
        &self,
        release: HelmRelease,
        context: &str,
    ) -> Result<ExitStatus, std::io::Error> {
        let mut install_command = &mut Command::new("helm");
        install_command = install_command
            .arg(format!("--kube-context={context}"))
            .arg("upgrade")
            .arg("--install")
            .arg(release.name)
            .arg(format!("{}/{}", release.chart.repo, release.chart.name))
            .arg(format!("--version={}", release.version));
        if !release.timeout.is_empty() {
            install_command = install_command.arg(format!("--timeout={}", release.timeout))
        }
        if !release.values.is_empty() {
            for value in release.values {
                install_command = install_command.arg("--set");
                install_command = install_command.arg(format!("{}={}", value.key, value.value));
            }
        }
        if !release.values_file.is_empty() {
            install_command = install_command.arg(format!("--values={}", release.values_file))
        }
        match install_command.status() {
            Ok(status) => Ok(status),
            Err(err) => Err(err),
        }
    }

    fn add_helm_repo(&self, name: &str, url: &str) -> Result<ExitStatus, std::io::Error> {
        let result = Command::new("helm")
            .arg("repo")
            .arg("add")
            .arg(name)
            .arg(url)
            .status();
        result
    }
}

impl<'a> KubernetesCluster for Minikube<'a> {
    fn create_cluster(&self, name: &str, version: &str, memory: &str, cpu: &str) {
        let list_command = Command::new("minikube")
            .arg("profile")
            .arg("list")
            .arg("-o=json")
            .output()
            .expect("cannot list minikube profiles");

        let profiles: MinikubeProfiles =
            serde_json::from_str(&String::from_utf8(list_command.stdout).expect("msg"))
                .expect("cannot convert minikube profile list output");
        for valid_profile in profiles.valid {
            if valid_profile.name == *name {
                return;
            }
        }

        println!("creating k8s cluster with version {version}, profile: {name}, cpu: {cpu}, memory: {memory}");

        let _ = Command::new("minikube")
            .arg("start")
            .arg(format!("--kubernetes-version={version}"))
            .arg(format!("--profile={name}"))
            .arg(format!("--cpus={cpu}"))
            .arg(format!("--memory={memory}"))
            .arg("--addons=ingress,dashboard,")
            .output()
            .expect("failed to create k8s cluster");
    }
    fn delete_cluster(&self, name: &str) {
        println!("deleting k8s cluster with profile: {name}");

        let _ = Command::new("minikube")
            .arg("delete")
            .arg(format!("--profile={name}"))
            .output()
            .expect("failed to delete k8s cluster");
    }
    fn get_auth(&self, name: String) -> String {
        name
    }
}

pub fn create_minikube_cluster(version: &str) -> String {
    let minikube = Minikube {
        //TODO: process addons correctly
        _addons: vec!["ingress", "ingress-dns", "dashboard"],
    };
    minikube.create_cluster(
        &String::from_str("minikube").expect("msg"),
        &String::from_str(version).expect("msg"),
        &String::from_str("8192Mb").expect("msg"),
        &String::from_str("4").expect("msg"),
    );
    minikube.get_auth(String::from_str("minikube").expect("msg"))
}

#[derive(Deserialize, Debug)]
struct MinikubeProfiles {
    #[serde(rename = "invalid")]
    _invalid: Vec<MinikubeProfile>,
    valid: Vec<MinikubeProfile>,
}

#[derive(Deserialize, Debug)]
struct MinikubeProfile {
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "Status")]
    _status: String,
    #[serde(rename = "Config")]
    _config: MinikubeProfileConfig,
}

#[derive(Deserialize, Debug)]
struct MinikubeProfileConfig {
    #[serde(rename = "Name")]
    _name: String,
    #[serde(rename = "KubernetesConfig")]
    _kubernetes_config: MinikubeProfileKubernetesConfig,
}

#[derive(Deserialize, Debug)]
struct MinikubeProfileKubernetesConfig {
    #[serde(rename = "KubernetesVersion")]
    _kubernetes_version: String,
    #[serde(rename = "ClusterName")]
    _cluster_name: String,
}
