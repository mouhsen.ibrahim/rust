use crate::kubernetes::{create_minikube_cluster, K8s, Kubernetes};
use std::collections::HashMap;
use std::process::Command;

use std::str::{self, FromStr};

use self::resources::{
    BindingRef, EventListener, EventListenerSpec, EventListenerTrigger, PipelineRun, TaskParam,
    TemplateRef, TriggerBinding, TriggerTemplate, TriggerTemplateParam, TriggerTemplateSpec,
};

mod resources {
    use serde::{Deserialize, Serialize};
    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct Task<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: TaskSpecs,
    }

    impl<'a> Task<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            spec: TaskSpecs,
        ) -> Task<'a> {
            Task {
                api_version: "tekton.dev/v1beta1",
                kind: "Task",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec,
            }
        }
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct Metadata<'a> {
        pub name: Option<&'a str>,
        #[serde(rename = "generateName")]
        pub generate_name: Option<&'a str>,
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct TaskSpecs {
        #[serde(rename = "Steps")]
        pub steps: Vec<Step>,
        pub params: Vec<PipelineParam>,
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct Step {
        pub name: String,
        pub image: String,
        pub script: String,
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct TaskRun<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: TaskRunSpecs,
    }

    impl<'a> TaskRun<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            task_name: &'a str,
        ) -> TaskRun<'a> {
            TaskRun {
                api_version: "tekton.dev/v1beta1",
                kind: "TaskRun",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec: TaskRunSpecs {
                    task_ref: TaskRef {
                        name: task_name.to_string(),
                    },
                },
            }
        }
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct TaskRunSpecs {
        #[serde(rename = "taskRef")]
        pub task_ref: TaskRef,
    }

    #[derive(Debug, Deserialize, Serialize, Default)]
    pub struct TaskRef {
        pub name: String,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct Pipeline<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: PipelineSpec,
    }

    impl<'a> Pipeline<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            spec: PipelineSpec,
        ) -> Pipeline<'a> {
            Pipeline {
                api_version: "tekton.dev/v1beta1",
                kind: "Pipeline",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec,
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineSpec {
        pub params: Vec<PipelineParam>,
        pub tasks: Vec<PipelineTask>,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineParam {
        pub name: String,
        #[serde(rename = "type")]
        pub param_type: String,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TaskParam {
        pub name: String,
        pub value: String,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineTask {
        pub name: String,
        #[serde(rename = "taskRef")]
        pub task_ref: TaskRef,
        #[serde(rename = "runAfter")]
        pub run_after: Option<Vec<String>>,
        pub params: Option<Vec<TaskParam>>,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineRun<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: PipelineRunSpec,
    }

    impl<'a> PipelineRun<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            pipeline_name: &'a str,
            params: Vec<TaskParam>,
        ) -> PipelineRun<'a> {
            PipelineRun {
                api_version: "tekton.dev/v1beta1",
                kind: "PipelineRun",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec: PipelineRunSpec {
                    pipeline_ref: PipelineRef {
                        name: pipeline_name.to_string(),
                    },
                    params,
                },
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineRunSpec {
        #[serde(rename = "pipelineRef")]
        pub pipeline_ref: PipelineRef,
        pub params: Vec<TaskParam>,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct PipelineRef {
        pub name: String,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TriggerTemplate<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: TriggerTemplateSpec<'a>,
    }

    impl<'a> TriggerTemplate<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            spec: TriggerTemplateSpec<'a>,
        ) -> TriggerTemplate<'a> {
            TriggerTemplate {
                api_version: "triggers.tekton.dev/v1beta1",
                kind: "TriggerTemplate",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec,
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TriggerTemplateSpec<'a> {
        pub params: Vec<TriggerTemplateParam>,
        #[serde(rename = "resourceTemplates", borrow)]
        pub resource_templates: Vec<PipelineRun<'a>>,
    }
    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TriggerTemplateParam {
        pub name: String,
        pub default: String,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TriggerBinding<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: TriggerBindingSpec,
    }

    impl<'a> TriggerBinding<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            params: Vec<TaskParam>,
        ) -> TriggerBinding<'a> {
            TriggerBinding {
                api_version: "triggers.tekton.dev/v1beta1",
                kind: "TriggerBinding",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec: TriggerBindingSpec { params },
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct TriggerBindingSpec {
        pub params: Vec<TaskParam>,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct EventListener<'a> {
        #[serde(rename = "apiVersion")]
        pub api_version: &'a str,
        pub kind: &'a str,
        pub metadata: Metadata<'a>,
        #[serde(rename = "Spec")]
        pub spec: EventListenerSpec,
    }

    impl<'a> EventListener<'a> {
        pub fn build(
            name: Option<&'a str>,
            generate_name: Option<&'a str>,
            spec: EventListenerSpec,
        ) -> EventListener<'a> {
            EventListener {
                api_version: "triggers.tekton.dev/v1beta1",
                kind: "EventListener",
                metadata: Metadata {
                    name,
                    generate_name,
                },
                spec,
            }
        }
    }

    #[derive(Default, Serialize, Deserialize, Debug)]
    pub struct EventListenerSpec {
        #[serde(rename = "serviceAccountName")]
        pub service_account_name: String,
        pub triggers: Vec<EventListenerTrigger>,
    }
    #[derive(Default, Serialize, Deserialize, Debug)]
    pub struct EventListenerTrigger {
        pub name: String,
        pub bindings: Vec<BindingRef>,
        pub template: TemplateRef,
    }
    #[derive(Default, Serialize, Deserialize, Debug)]
    pub struct BindingRef {
        #[serde(rename = "ref")]
        pub ref_name: String,
    }
    #[derive(Default, Serialize, Deserialize, Debug)]
    pub struct TemplateRef {
        #[serde(rename = "ref")]
        pub ref_name: String,
    }
}

pub fn tekton_run() {
    create_minikube_cluster("1.30.0");
    let _ = Command::new("kubectl")
        .arg("apply")
        .arg("--filename")
        .arg("https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml")
        .status()
        .expect("cannot install tekton");
    let _ = Command::new("kubectl")
        .arg("apply")
        .arg("--filename")
        .arg("https://storage.googleapis.com/tekton-releases/dashboard/latest/release.yaml")
        .status()
        .expect("cannot install tekton");
}

pub fn tekton_trigger_run() {
    create_minikube_cluster("1.30.0");
    let _ = Command::new("kubectl")
        .arg("apply")
        .arg("--filename")
        .arg("https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml")
        .status()
        .expect("cannot install tekton");
    let _ = Command::new("kubectl")
        .arg("apply")
        .arg("--filename")
        .arg("https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml")
        .status()
        .expect("cannot install tekton");
}

fn create_task(name: &str, image: &str, script: &str, params: Vec<resources::PipelineParam>) {
    let tekton_task_spec = resources::TaskSpecs {
        steps: vec![resources::Step {
            name: String::from_str("step1").expect("msg"),
            image: String::from_str(image).expect(""),
            script: String::from_str(script).expect("msg"),
        }],
        params,
    };
    let tekton_task = resources::Task::build(Some(name), None, tekton_task_spec);
    let k8s = K8s {};
    k8s.apply(tekton_task);
}

fn create_task_run(name: &str, task_name: &str) {
    let tekton_task_run = resources::TaskRun::build(Some(name), None, task_name);
    let k8s = K8s {};
    k8s.apply(tekton_task_run);
}

pub fn tekton_task() {
    create_task(
        "hello",
        "alpine",
        "#!/bin/sh
    echo \"Hello World\" ",
        vec![],
    )
}

pub fn tekton_task_run() {
    create_task_run("hello-task-run", "hello")
}

pub fn tekton_pipeline_create() {
    create_task(
        "goodbye",
        "ubuntu",
        "#!/bin/bash
    echo \"Goodbye $(params.username)!\"",
        vec![resources::PipelineParam {
            name: "username".to_string(),
            param_type: "string".to_string(),
        }],
    );
    create_task(
        "hello",
        "alpine",
        "#!/bin/sh
    echo \"Hello World\" ",
        vec![],
    );
    let pipeline_spec = resources::PipelineSpec {
        params: vec![resources::PipelineParam {
            name: "username".to_string(),
            param_type: "string".to_string(),
        }],
        tasks: vec![
            resources::PipelineTask {
                name: "hello".to_string(),
                task_ref: resources::TaskRef {
                    name: "hello".to_string(),
                },
                ..Default::default()
            },
            resources::PipelineTask {
                name: "goodbye".to_string(),
                task_ref: resources::TaskRef {
                    name: "goodbye".to_string(),
                },
                run_after: Some(vec!["hello".to_string()]),
                params: Some(vec![resources::TaskParam {
                    name: "username".to_string(),
                    value: "$(params.username)".to_string(),
                }]),
            },
        ],
    };
    let pipeline = resources::Pipeline::build(Some("hello-goodbye"), None, pipeline_spec);
    let k8s = K8s {};
    k8s.apply(pipeline);
}

pub fn tekton_pipeline_run_create() {
    let pipeline_run = resources::PipelineRun::build(
        Some("hello-goodbye-run"),
        None,
        "hello-goodbye",
        vec![resources::TaskParam {
            name: "username".to_string(),
            value: "tekton".to_string(),
        }],
    );
    let k8s = K8s {};
    k8s.apply(pipeline_run);
}

pub fn tekton_port_forward() {
    println!("access tekton dashboard at this URL http://localhost:9097");
    let _ = Command::new("kubectl")
        .arg("port-forward")
        .arg("service/tekton-dashboard")
        .arg("-n")
        .arg("tekton-pipelines")
        .arg("9097:9097")
        .status()
        .expect("cannot port forward tekton dashboad");
}

pub fn tekton_trigger_template() {
    let pipeline_run = PipelineRun::build(
        None,
        Some("hello-goodbye-run-"),
        "hello-goodbye",
        vec![TaskParam {
            name: "username".to_string(),
            value: "$(tt.params.username)".to_string(),
        }],
    );
    let trigger_template_spec = TriggerTemplateSpec {
        params: vec![TriggerTemplateParam {
            name: "username".to_string(),
            default: "Kubernetes".to_string(),
        }],
        resource_templates: vec![pipeline_run],
    };
    let trigger_template =
        TriggerTemplate::build(Some("hello-template"), None, trigger_template_spec);
    let k8s = K8s {};
    k8s.apply(trigger_template);
}

pub fn tekton_trigger_binding() {
    let trigger_binding = TriggerBinding::build(
        Some("hello-binding"),
        None,
        vec![TaskParam {
            name: "username".to_string(),
            value: "$(body.username)".to_string(),
        }],
    );
    let k8s = K8s {};
    k8s.apply(trigger_binding);
}

pub async fn tekton_event_listener() {
    let event_listener_spec = EventListenerSpec {
        service_account_name: "tekton-robot".to_string(),
        triggers: vec![EventListenerTrigger {
            name: "hello-trigger".to_string(),
            bindings: vec![BindingRef {
                ref_name: "hello-binding".to_string(),
            }],
            template: TemplateRef {
                ref_name: "hello-template".to_string(),
            },
        }],
    };
    let event_listener = EventListener::build(Some("hello-listener"), None, event_listener_spec);
    let k8s = K8s {};
    k8s.apply(event_listener);

    k8s.create_sa(
        "tekton-robot".to_string(),
        "default".to_string(),
        "minikube".to_string(),
    )
    .await;
    k8s.create_role_binding(
        "triggers-example-eventlistener-binding".to_string(),
        "default".to_string(),
        "minikube".to_string(),
        "tekton-robot".to_string(),
        "tekton-triggers-eventlistener-roles".to_string(),
    )
    .await;
    k8s.create_cluster_role_binding(
        "triggers-example-eventlistener-binding".to_string(),
        "minikube".to_string(),
        "tekton-robot".to_string(),
        "tekton-triggers-eventlistener-clusterroles".to_string(),
    )
    .await;
}

pub fn tekton_event_port_forward() {
    println!("access event listener at this URL http://localhost:8080");
    let _ = Command::new("kubectl")
        .arg("port-forward")
        .arg("service/el-hello-listener")
        .arg("-n")
        .arg("default")
        .arg("8080:8080")
        .status()
        .expect("cannot port forward event listener");
}

pub async fn tekton_trigger_test(name: &str) {
    let mut data = HashMap::new();
    data.insert("username", name);
    let client = reqwest::Client::new();
    let res = client.get("http://localhost:8080").json(&data).send().await;
    println!("response is {:?}", res)
}
