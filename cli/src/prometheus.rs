use crate::{
    errors::Error,
    kubernetes::{create_minikube_cluster, Helm, HelmChart, HelmRelease, Helmer},
};
use std::process::Command;

use crate::errors;

pub fn prometheus_run() -> Result<(), errors::Error<std::io::Error>> {
    let context = create_minikube_cluster("1.30.0");
    let helm = Helm {};
    match helm.add_helm_repo(
        "prometheus-community",
        "https://prometheus-community.github.io/helm-charts",
    ) {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    let release = HelmRelease {
        chart: HelmChart {
            name: "prometheus",
            repo: "prometheus-community",
        },
        name: "my-prometheus",
        version: "25.20.1",
        timeout: "100s",
        values: vec![],
        values_file: "",
    };
    match helm.install_helm_chart(release, context.as_str()) {
        Ok(_) => {}
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    Ok(())
}

pub fn prometheus_port_forward() -> Result<(), Error<std::io::Error>> {
    println!("access prometheus at this URL http://localhost:9090");
    match Command::new("kubectl")
        .arg("port-forward")
        .arg("service/my-prometheus-server")
        .arg("-n")
        .arg("default")
        .arg("9090:80")
        .status()
    {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    Ok(())
}
