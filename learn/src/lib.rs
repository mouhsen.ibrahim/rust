use serde::{Deserialize, Serialize};

use std::io::Write;
use std::{collections::HashMap, fs::File};

use k8s_openapi::api::core::v1::Secret;

use kube::{api::ListParams, Api, Client};

#[derive(Debug, Serialize, Deserialize)]
pub enum Gender {
    Male,
    Female,
}

pub trait JsonFormatter {
    fn format(&self) -> serde_json::Result<String>;
}

pub trait YamlFormatter {
    fn format_yaml(&self) -> serde_yaml::Result<String>;
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Person {
    pub age: i8,
    pub gender: Gender,
    pub name: String,
}

impl Person {
    pub fn introduce(&self) -> String {
        format!(
            "Hi, My name is {}, I am {:?} and {} years old",
            self.name, self.gender, self.age
        )
    }
}

#[cfg(test)]
mod tests {

    use crate::{JsonFormatter, Person, YamlFormatter};

    #[test]
    fn person_introduce() {
        let p = Person {
            age: 33,
            gender: crate::Gender::Male,
            name: "mouhsen".to_string(),
        };
        let introduce = p.introduce();
        assert_eq!(
            introduce,
            "Hi, My name is mouhsen, I am Male and 33 years old".to_string()
        );
    }

    #[test]
    fn person_yaml_format() {
        let p = Person {
            age: 33,
            gender: crate::Gender::Male,
            name: "mouhsen".to_string(),
        };
        let out = p.format_yaml();
        assert_eq!(
            out.expect("error in format_yaml call"),
            "age: 33\ngender: Male\nname: mouhsen\n"
        )
    }
    #[test]
    fn person_josn_format() {
        let p = Person {
            age: 33,
            gender: crate::Gender::Male,
            name: "mouhsen".to_string(),
        };
        let out = p.format();
        assert_eq!(
            out.expect("error in format call"),
            "{\"age\":33,\"gender\":\"Male\",\"name\":\"mouhsen\"}"
        )
    }
}

impl JsonFormatter for Person {
    fn format(&self) -> serde_json::Result<String> {
        let j = serde_json::to_string(self)?;
        serde_json::Result::Ok(j)
    }
}

impl YamlFormatter for Person {
    fn format_yaml(&self) -> serde_yaml::Result<String> {
        let y = serde_yaml::to_string(self)?;
        serde_yaml::Result::Ok(y)
    }
}

#[derive(Debug, Serialize)]
pub struct OpenStackCreds {
    auth_url: String,
    username: String,
    password: String,
    domain_name: String,
    tenant_name: String,
    region: String,
}

impl YamlFormatter for OpenStackCreds {
    fn format_yaml(&self) -> serde_yaml::Result<String> {
        let y = serde_yaml::to_string(self)?;
        serde_yaml::Result::Ok(y)
    }
}

pub fn new_openstack_creds(global: &HashMap<String, String>) -> OpenStackCreds {
    let auth_url = global.get("auth-url").expect("no auth url").clone();
    let username = global.get("username").expect("no username").clone();
    let password = global.get("password").expect("no password").clone();
    let domain_name = global.get("domain-name").expect("no domain-name").clone();
    let tenant_name = global.get("tenant-name").expect("no tenant-name").clone();
    let region = global.get("region").expect("no region").clone();
    OpenStackCreds {
        auth_url,
        username,
        password,
        domain_name,
        tenant_name,
        region,
    }
}

pub async fn k8s() {
    let client = Client::try_default()
        .await
        .expect("cannot create default k8s client");

    let secrets: Api<Secret> = Api::namespaced(client, "default");
    let secrets = secrets
        .list(&ListParams::default())
        .await
        .expect("cannot list secrets");
    for secret in secrets {
        println!("secret name is {}", secret.metadata.name.expect("msg"));
        let data = secret.data.expect("data");
        let file = data
            .get("cloudprovider.conf")
            .expect("cannot find cloudprovider.conf:");
        let file_data = std::str::from_utf8(&file.0).expect("msg");
        let ini_data: HashMap<String, HashMap<String, String>> =
            serde_ini::from_str(file_data).expect("msg");
        let global = ini_data.get("Global").expect("msg");
        let os_creds = new_openstack_creds(global);
        let js = os_creds.format_yaml().expect("msg");
        let mut out = File::create("out.yaml").expect("cannot create file");
        write!(out, "{js}").expect("cannot write to a file");
    }
}
