rust-install:
	curl https://sh.rustup.rs -sSf | bash -s -- -y

tools:
	rustup component add clippy

lint:
	cargo clippy --all-targets --all-features -- -D warnings

fix:
	cargo clippy --all-targets --all-features --fix -- -D warnings

test:
	cargo test

TAG?=latest

build-service-docker:
	docker build -t mouhsen47/rust-service:${TAG} . -f ./service_test/Dockerfile

publish-service-docker: build-service-docker
	docker push mouhsen47/rust-service
