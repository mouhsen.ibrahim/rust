use crate::errors::{self, Error};
use crate::kubernetes::{
    create_minikube_cluster, Helm, HelmChart, HelmRelease, Helmer, K8s, Kubernetes,
};
use crate::my_gitlab::{create_deploy_key, DeployKey, Gitlab};

use k8s_openapi::api::core::v1::Secret;
use ssh_key::PrivateKey;
use std::collections::BTreeMap;
use std::env;
use std::path::Path;
use std::process::Command;
use std::str::{self, FromStr};

use kube::{config::KubeConfigOptions, Api, Client, Config};

use crate::utils::create_ssh_key_pair;
use serde::{Deserialize, Serialize};

pub fn run_argocd() -> Result<(), errors::Error<std::io::Error>> {
    let context = create_minikube_cluster("1.30.0");
    let helm = Helm {};
    match helm.add_helm_repo("argo", "https://argoproj.github.io/argo-helm") {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    let release = HelmRelease {
        chart: HelmChart {
            name: "argo-cd",
            repo: "argo",
        },
        name: "my-argo-cd",
        version: "6.7.17",
        timeout: "100s",
        values: vec![],
        values_file: "",
    };
    match helm.install_helm_chart(release, context.as_str()) {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    Ok(())
}

pub async fn argocd_print_password(context: &str) -> Result<(), errors::Error<kube::error::Error>> {
    let kube_config_options = KubeConfigOptions {
        context: Some(String::from_str(context).expect("msg")),
        ..Default::default()
    };
    let config = Config::from_kubeconfig(&kube_config_options).await?;
    let client = Client::try_from(config).expect("cannot create default k8s client");

    let secrerts: Api<Secret> = Api::namespaced(client, "default");
    let secret = secrerts.get("argocd-initial-admin-secret").await?;
    println!(
        "password: {:?}",
        str::from_utf8(&secret.data.expect("").get("password").expect("msg").0).expect("msg")
    );
    Ok(())
}

pub async fn argocd_port_forward(context: &str) -> Result<(), errors::Error<kube::error::Error>> {
    println!("access argocd at this URL https://localhost:8080 using this password");
    argocd_print_password(context).await?;
    let _ = Command::new("kubectl")
        .arg("port-forward")
        .arg("service/my-argo-cd-argocd-server")
        .arg("-n")
        .arg("default")
        .arg("8080:443")
        .status()?;
    Ok(())
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct Metadata<'a> {
    pub name: Option<&'a str>,
    #[serde(rename = "generateName")]
    pub generate_name: Option<&'a str>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Application<'a> {
    #[serde(rename = "apiVersion")]
    pub api_version: &'a str,
    pub kind: &'a str,
    pub metadata: Metadata<'a>,
    pub spec: ApplicationSpec,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ApplicationSpec {
    destination: Destination,
    project: String,
    source: Source,
    #[serde(rename = "syncPolicy")]
    sync_policy: SyncPolicy,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Destination {
    pub server: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Source {
    pub path: String,
    #[serde(rename = "repoURL")]
    pub repo_url: String,
    #[serde(rename = "targetRevision")]
    pub target_revision: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct SyncPolicy {
    pub automated: Automated,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Automated {
    pub prune: bool,
    #[serde(rename = "selfHeal")]
    pub self_heal: bool,
}

pub async fn argocd_create_app(repository_url: String, path: String) {
    create_ssh_key_pair(
        &".secrets/id_rsa.pub".to_string(),
        &".secrets/id_rsa".to_string(),
    )
    .expect("cannot create private/public key pair");
    let k8s = K8s {};
    let mut data = BTreeMap::new();
    data.insert("name".to_string(), "manifests-repo".to_string());
    data.insert("project".to_string(), "default".to_string());
    data.insert("type".to_string(), "git".to_string());
    data.insert("url".to_string(), repository_url.clone());
    let private_key = PrivateKey::read_openssh_file(Path::new(".secrets/id_rsa"))
        .expect("cannot read private key file");
    data.insert(
        "sshPrivateKey".to_string(),
        private_key
            .to_openssh(ssh_key::LineEnding::LF)
            .expect("msg")
            .as_str()
            .to_string(),
    );
    k8s.create_secret(
        "manifests-test".to_string(),
        "default".to_string(),
        data,
        "minikube".to_string(),
    )
    .await;
    let public_key = private_key.public_key();
    let token = env::var("GITLAB_TOKEN").expect("msg");

    // Create the client.
    let client = gitlab::Gitlab::builder("gitlab.com", token)
        .build_async()
        .await
        .expect("msg");

    let _gitlab = Gitlab { client };
    create_deploy_key(
        DeployKey {
            key: public_key.to_string(),
            project: Some("mouhsen.ibrahim/argocd-manifests-test".to_string()),
            ..Default::default()
        },
        _gitlab,
    )
    .await;

    let app = Application {
        api_version: "argoproj.io/v1alpha1",
        kind: "Application",
        metadata: Metadata {
            name: Some("test"),
            ..Default::default()
        },
        spec: ApplicationSpec {
            destination: Destination {
                server: "https://kubernetes.default.svc".to_string(),
            },
            project: "default".to_string(),
            source: Source {
                path,
                repo_url: repository_url,
                target_revision: "main".to_string(),
            },
            sync_policy: SyncPolicy {
                automated: Automated {
                    prune: true,
                    self_heal: true,
                },
            },
        },
    };
    let k8s = K8s {};
    k8s.apply(app);
}
