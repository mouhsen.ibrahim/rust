use std::{
    io::{Error, Write},
    process::Command,
};

use reqwest::{self, Client};
use serde::Deserialize;
use serde_json::error;
use tempfile::NamedTempFile;

pub trait DevOps {
    async fn get_pipelines(&self) -> Result<ListResponse<Pipeline>, error::Error>;
    async fn get_runs(&self, pipeline_id: i32) -> Result<ListResponse<Run>, error::Error>;
    async fn get_logs(&self, pipeline_id: i32, run_id: i32) -> Result<LogsResponse, error::Error>;
    async fn search_logs(
        &self,
        pipeline_id: i32,
        run_id: i32,
        text: &[String],
    ) -> Result<String, Error>;
    async fn save_logs(&self, pipeline_id: i32, run_id: i32) -> Result<String, Error>;
}

pub struct DevOpsClient<'a> {
    pub token: &'a str,
    pub base_url: &'a str,
    pub org: &'a str,
    pub project: &'a str,
}

#[derive(Default, Deserialize, Debug)]
pub struct ListResponse<T> {
    #[serde(rename = "count")]
    _count: i32,
    #[serde(rename = "value")]
    pub value: Vec<T>,
}

#[derive(Default, Deserialize, Debug)]
pub struct Pipeline {
    #[serde(rename = "id")]
    _id: i32,
    #[serde(rename = "name")]
    _name: String,
}

#[derive(Default, Deserialize, Debug)]
pub struct Run {
    #[serde(rename = "id")]
    pub id: i32,
    #[serde(rename = "name")]
    _name: String,
    #[serde(rename = "url")]
    _url: String,
}

#[derive(Default, Deserialize, Debug)]
pub struct Log {
    #[serde(rename = "id")]
    _id: i32,
    #[serde(rename = "lineCount")]
    _line_count: i32,
}

#[derive(Default, Deserialize, Debug)]
pub struct SignedUrl {
    url: String,
}

#[derive(Default, Deserialize, Debug)]
pub struct LogsResponse {
    pub logs: Vec<Log>,
    #[serde(rename = "signedContent")]
    pub signed_content: SignedUrl,
}

impl<'a> DevOps for DevOpsClient<'a> {
    async fn get_pipelines(&self) -> Result<ListResponse<Pipeline>, error::Error> {
        let c = Client::builder().build().expect("msg");
        let url = format!(
            "{}/{}/{}/_apis/pipelines?api-version=7.1-preview.1",
            self.base_url, self.org, self.project
        );
        let req = c
            .get(url)
            .basic_auth("", Some(self.token))
            .build()
            .expect("msg");
        let body = c
            .execute(req)
            .await
            .expect("msg")
            .text()
            .await
            .expect("msg");
        let response: ListResponse<Pipeline> = serde_json::from_str(&body).expect("msg");
        Ok(response)
    }
    async fn get_runs(&self, pipeline_id: i32) -> Result<ListResponse<Run>, error::Error> {
        let c = Client::builder().build().expect("msg");
        let url = format!(
            "{}/{}/{}/_apis/pipelines/{}/runs?api-version=6.0-preview.1",
            self.base_url, self.org, self.project, pipeline_id
        );
        let req = c
            .get(url)
            .basic_auth("", Some(self.token))
            .build()
            .expect("msg");
        let body = c
            .execute(req)
            .await
            .expect("msg")
            .text()
            .await
            .expect("msg");
        let response: ListResponse<Run> = serde_json::from_str(&body).expect("msg");
        Ok(response)
    }
    async fn get_logs(&self, pipeline_id: i32, run_id: i32) -> Result<LogsResponse, error::Error> {
        let c = Client::builder().build().expect("msg");
        let url = format!(
            "{}/{}/{}/_apis/pipelines/{}/runs/{}/logs?$expand=signedContent&api-version=7.1-preview.1",
            self.base_url, self.org, self.project, pipeline_id, run_id
        );
        let req = c
            .get(url)
            .basic_auth("", Some(self.token))
            .build()
            .expect("msg");
        let body = c
            .execute(req)
            .await
            .expect("msg")
            .text()
            .await
            .expect("msg");
        let response = serde_json::from_str(&body).expect("msg");
        Ok(response)
    }
    async fn save_logs(&self, pipeline_id: i32, run_id: i32) -> Result<String, Error> {
        let logs = self.get_logs(pipeline_id, run_id).await?;
        let logs_url = logs.signed_content.url;
        let body = reqwest::get(logs_url)
            .await
            .expect("msg")
            .bytes()
            .await
            .expect("msg");
        let mut tmpfile = NamedTempFile::new().unwrap();
        let _ = tmpfile.write(&body);
        let _ = Command::new("unzip")
            .arg(tmpfile.path().to_str().expect("msg"))
            .arg(format!("-d.logs/logs_{}_{}", pipeline_id, run_id))
            .output()
            .expect("msg");
        Ok(tmpfile.path().to_str().expect("msg").to_string())
    }
    async fn search_logs(
        &self,
        pipeline_id: i32,
        run_id: i32,
        text: &[String],
    ) -> Result<String, Error> {
        let logs = self.get_logs(pipeline_id, run_id).await?;
        let logs_url = logs.signed_content.url;
        let body = reqwest::get(logs_url)
            .await
            .expect("msg")
            .bytes()
            .await
            .expect("msg");
        let mut tmpfile = NamedTempFile::new().unwrap();
        let _ = tmpfile.write(&body);
        let _ = Command::new("unzip")
            .arg(tmpfile.path().to_str().expect("msg"))
            .arg(format!("-d.logs/logs_{}_{}", pipeline_id, run_id))
            .output()
            .expect("msg");
        for t in text {
            let command = Command::new("grep")
                .arg(t)
                .arg("-R")
                .arg(format!(".logs/logs_{}_{}", pipeline_id, run_id))
                .status()
                .expect("msg");
            if command.success() {
                println!("random failure detected: {t}")
            } else {
                println!("no random failure detected")
            }
        }
        Ok(tmpfile.path().to_str().expect("msg").to_string())
    }
}
