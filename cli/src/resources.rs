pub trait Resource<T> {
    fn same_id(&self, other: &Self) -> bool;
    fn get_id(&self) -> Option<T>;
}

#[cfg(test)]
mod tests {
    use super::*;

    struct TestResource {
        id: i64,
    }

    impl Resource<i64> for TestResource {
        fn get_id(&self) -> Option<i64> {
            Some(self.id)
        }
        fn same_id(&self, other: &Self) -> bool {
            self.id == other.id
        }
    }

    #[test]
    fn test_resource() {
        let t1 = TestResource { id: 1 };
        let t2 = TestResource { id: 1 };
        let t3 = TestResource { id: 2 };
        assert_eq!(t1.get_id(), Some(1));
        assert!(t1.same_id(&t2));
        assert!(!t1.same_id(&t3));
    }
}
