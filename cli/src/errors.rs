use gitlab::{api::ApiError, RestError};
use kube::config::KubeconfigError;

#[derive(Debug)]
pub struct Error<T: std::error::Error> {
    pub kind: ErrorKind,
    pub err: Option<T>,
}

#[derive(Debug, PartialEq)]
pub enum ErrorKind {
    CommandError,
    NotFound,
    Other,
}

impl From<KubeconfigError> for Error<kube::error::Error> {
    fn from(_value: KubeconfigError) -> Self {
        Error {
            kind: ErrorKind::CommandError,
            err: None,
        }
    }
}

impl From<kube::error::Error> for Error<kube::error::Error> {
    fn from(value: kube::error::Error) -> Self {
        Error {
            kind: ErrorKind::CommandError,
            err: Some(value),
        }
    }
}

impl From<std::io::Error> for Error<kube::error::Error> {
    fn from(_value: std::io::Error) -> Self {
        Error {
            kind: ErrorKind::CommandError,
            err: None,
        }
    }
}

impl From<ApiError<RestError>> for Error<ApiError<RestError>> {
    fn from(value: ApiError<RestError>) -> Self {
        match value {
            ApiError::Gitlab { ref msg } => {
                if msg == "404 Not found" {
                    Error {
                        kind: ErrorKind::NotFound,
                        err: Some(value),
                    }
                } else {
                    Error {
                        kind: ErrorKind::Other,
                        err: Some(value),
                    }
                }
            }
            _ => Error {
                kind: ErrorKind::Other,
                err: Some(value),
            },
        }
    }
}

impl From<gitlab::api::projects::deploy_keys::CreateDeployKeyBuilderError>
    for Error<kube::error::Error>
{
    fn from(_value: gitlab::api::projects::deploy_keys::CreateDeployKeyBuilderError) -> Self {
        Error {
            kind: ErrorKind::CommandError,
            err: None,
        }
    }
}
