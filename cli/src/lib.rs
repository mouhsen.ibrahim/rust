use std::{env, str};

use gitlab::api::{
    self,
    projects::{self, issues},
    AsyncQuery,
};
use gitlab::Gitlab;

use serde::Deserialize;

// The return type of a `Project`. Note that GitLab may contain more information, but you can
// define your structure to only fetch what is needed.
#[derive(Debug, Deserialize)]
struct Project {
    #[allow(dead_code)]
    name: String,
}

#[derive(Debug, Deserialize)]
struct Issue {
    title: String,
}

pub async fn create_topic(title: &String, link: &String) {
    let token = env::var("GITLAB_TOKEN").expect("msg");

    // Create the client.
    let client = Gitlab::builder("gitlab.com", token)
        .build_async()
        .await
        .expect("msg");

    let issues_endpoint = issues::Issues::builder()
        .project("mouhsen.ibrahim/learning")
        .build()
        .expect("msg");
    let issues: Vec<Issue> = api::paged(issues_endpoint, api::Pagination::All)
        .query_async(&client)
        .await
        .expect("msg");
    for issue in issues {
        if title.contains(&issue.title) {
            return;
        }
    }
    let issues_endpoint = projects::issues::CreateIssue::builder()
        .project("mouhsen.ibrahim/learning")
        .title(title)
        .description(format!("learn about [{}]({})", title, link))
        .label("idea:new")
        .build()
        .expect("msg");
    let issue: Issue = issues_endpoint.query_async(&client).await.expect("msg");
    println!("Issue created is {:?}", issue)
}
