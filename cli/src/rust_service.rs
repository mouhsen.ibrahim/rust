use k8s_openapi::api::apps::v1::{Deployment, DeploymentSpec};
use k8s_openapi::api::core::v1::{Container, ContainerPort, PodSpec, PodTemplateSpec};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::LabelSelector;
use kube::api::{ObjectMeta, PostParams};
use kube::{config::KubeConfigOptions, Api, Client, Config};
use std::collections::BTreeMap;
use std::{env, io::Error, process::Command};

pub trait Service {
    fn build(&self, tag: &str) -> Result<String, Error>;
    async fn deploy(&self, tag: &str, context: String);
}

pub struct RustService {}

impl Service for RustService {
    fn build(&self, tag: &str) -> Result<String, Error> {
        env::set_var("TAG", tag);
        let _ = Command::new("make")
            .arg("-C")
            .arg("..")
            .arg("build-service-docker")
            .status()?;
        Ok(format!("mouhsen47/rust-service:{}", tag))
    }
    async fn deploy(&self, tag: &str, context: String) {
        let _ = Command::new("minikube")
            .arg("image")
            .arg("load")
            .arg(format!("mouhsen47/rust-service:{tag}"))
            .status()
            .expect("cannot load image to minikube");
        let kube_config_options = KubeConfigOptions {
            context: Some(context.clone()),
            ..Default::default()
        };
        let config = Config::from_kubeconfig(&kube_config_options)
            .await
            .expect("msg");
        let client = Client::try_from(config).expect("cannot create default k8s client");

        let mut selector = BTreeMap::new();
        selector.insert("app".to_string(), "rust-service".to_string());
        let mut annotations = BTreeMap::new();
        annotations.insert("prometheus.io/scrape".to_string(), "true".to_string());
        annotations.insert("prometheus.io/path".to_string(), "/metrics".to_string());
        annotations.insert("prometheus.io/port".to_string(), "8080".to_string());

        let deploy = Deployment {
            metadata: ObjectMeta {
                name: Some("rust-service".to_string()),
                namespace: Some("default".to_string()),
                ..Default::default()
            },
            spec: Some(DeploymentSpec {
                selector: LabelSelector {
                    match_labels: Some(selector.clone()),
                    ..Default::default()
                },
                template: PodTemplateSpec {
                    metadata: Some(ObjectMeta {
                        labels: Some(selector),
                        annotations: Some(annotations),
                        ..Default::default()
                    }),
                    spec: Some(PodSpec {
                        containers: vec![Container {
                            name: "rust-service".to_string(),
                            image: Some(format!("mouhsen47/rust-service:{tag}").to_string()),
                            ports: Some(vec![ContainerPort {
                                container_port: 8080,
                                name: Some("http".to_string()),
                                protocol: Some("TCP".to_string()),
                                ..Default::default()
                            }]),
                            ..Default::default()
                        }],
                        ..Default::default()
                    }),
                },
                ..Default::default()
            }),
            ..Default::default()
        };

        let deployments: Api<Deployment> = Api::namespaced(client, "default");
        deployments
            .create(&PostParams::default(), &deploy)
            .await
            .expect("cannot create deployment");
    }
}
