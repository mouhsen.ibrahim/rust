use std::process::Command;

use crate::errors::{self, Error, ErrorKind};
use crate::kubernetes::{create_minikube_cluster, Helm, HelmChart, HelmRelease, HelmValue, Helmer};
use crate::resources::Resource;

use gitlab::api::projects::deploy_keys::{self};
use k8s_openapi::api::core::v1::Secret;
use kube::{config::KubeConfigOptions, Api, Client, Config};
use std::str::{self, FromStr};

use gitlab::api::{ApiError, AsyncQuery};
use gitlab::{AsyncGitlab, RestError};

use serde::Deserialize;

pub fn run_gitlab() -> Result<(), errors::Error<std::io::Error>> {
    let command = Command::new("minikube")
        .arg("ip")
        .output()
        .expect("cannot get minikube ip");
    let ip = String::from_utf8(command.stdout).expect("cannot convert minikube ip command output");
    let ip_value = format!("{}.nip.io", ip.trim());
    let context = create_minikube_cluster("1.30.0");
    let helm = Helm {};
    match helm.add_helm_repo("gitlab", "https://charts.gitlab.io/") {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    let release = HelmRelease {
        chart: HelmChart {
            name: "gitlab",
            repo: "gitlab",
        },
        name: "my-gitlab",
        timeout: "300s",
        values_file: "https://gitlab.com/gitlab-org/charts/gitlab/raw/master/examples/values-minikube-minimum.yaml",
        values: vec![HelmValue{
            key: "global.hosts.domain",
            value: ip_value.as_str(),
        }, HelmValue {
            key: "global.hosts.externalIP",
            value: ip.trim(),
        }],
        version: "v7.9.6",
    };
    match helm.install_helm_chart(release, context.as_str()) {
        Ok(status) => {
            if status.code() != Some(0) {
                return Err(Error {
                    kind: errors::ErrorKind::CommandError,
                    err: None,
                });
            }
        }
        Err(err) => {
            return Err(Error {
                kind: errors::ErrorKind::CommandError,
                err: Some(err),
            })
        }
    }
    Ok(())
}

pub async fn gitlab_print_password(context: &str) -> Result<(), Error<kube::error::Error>> {
    let kube_config_options = KubeConfigOptions {
        context: Some(String::from_str(context).expect("msg")),
        ..Default::default()
    };
    let config = Config::from_kubeconfig(&kube_config_options).await?;
    let client = Client::try_from(config)?;

    let secrerts: Api<Secret> = Api::namespaced(client, "default");
    let secret = secrerts
        .get("my-gitlab-gitlab-initial-root-password")
        .await?;
    println!(
        "password: {:?}",
        str::from_utf8(&secret.data.expect("").get("password").expect("msg").0).expect("msg")
    );
    Ok(())
}

pub struct Gitlab {
    pub client: AsyncGitlab,
}

impl Gitlab {
    pub async fn list_deploy_keys(
        &self,
        deploy_key: &DeployKey,
    ) -> Result<Vec<DeployKey>, Error<ApiError<RestError>>> {
        let project = deploy_key.project.clone();
        let project_deploy_keys_endpoint = deploy_keys::DeployKeys::builder()
            .project(project.expect("msg"))
            .build()
            .expect("msg");
        let deploy_keys: Vec<DeployKey> = project_deploy_keys_endpoint
            .query_async(&self.client)
            .await?;
        Ok(deploy_keys)
    }
    pub async fn update_deploy_key(
        &self,
        deploy_key: &DeployKey,
    ) -> Result<Option<DeployKey>, Error<ApiError<RestError>>> {
        let project = deploy_key.project.clone();
        let title = deploy_key.title.clone();
        let project_deploy_keys_endpoint = deploy_keys::EditDeployKey::builder()
            .project(project.expect("msg"))
            .can_push(deploy_key.can_push)
            .title(title)
            .deploy_key(deploy_key.id.expect("msg"))
            .build()
            .expect("msg");
        let deploy_key: DeployKey = project_deploy_keys_endpoint
            .query_async(&self.client)
            .await
            .expect("msg");
        Ok(Some(deploy_key))
    }
    pub async fn create_deploy_key(
        &self,
        deploy_key: &DeployKey,
    ) -> Result<Option<DeployKey>, Error<ApiError<RestError>>> {
        let project = deploy_key.project.clone();
        let title = deploy_key.title.clone();
        let key = deploy_key.key.clone();
        let project_deploy_keys_endpoint = deploy_keys::CreateDeployKey::builder()
            .project(project.expect("msg"))
            .can_push(deploy_key.can_push)
            .title(title)
            .key(key)
            .build()
            .expect("msg");
        let deploy_key: DeployKey = project_deploy_keys_endpoint
            .query_async(&self.client)
            .await
            .expect("msg");
        Ok(Some(deploy_key))
    }
}

#[derive(Deserialize, Debug, Default, Clone)]
pub struct DeployKey {
    pub id: Option<u64>,
    pub title: String,
    pub key: String,
    pub project: Option<String>,
    pub can_push: bool,

    #[serde(skip)]
    pub client: Option<AsyncGitlab>,
}

impl PartialEq for DeployKey {
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key && self.can_push == other.can_push && self.title == other.title
    }
}

impl Resource<u64> for DeployKey {
    fn same_id(&self, other: &Self) -> bool {
        if self.id.is_none() || other.id.is_none() {
            return self.key == other.key;
        }
        self.id == other.id
    }
    fn get_id(&self) -> Option<u64> {
        self.id
    }
}

impl DeployKey {
    pub async fn get(&self) -> Result<Option<DeployKey>, Error<ApiError<RestError>>> {
        let client = self.client.clone().expect("msg");
        let project = self.project.clone();
        let project_deploy_keys_endpoint = deploy_keys::DeployKey::builder()
            .project(project.expect("msg"))
            .deploy_key(self.id.expect("msg"))
            .build()
            .expect("msg");
        let deploy_key = project_deploy_keys_endpoint.query_async(&client).await?;
        Ok(deploy_key)
    }
}

pub async fn create_deploy_key(mut key: DeployKey, _gitlab: Gitlab) {
    let id = key.get_id();

    let mut create = true;
    let mut deploy_key: Option<DeployKey> = None;
    match id {
        Some(_) => {
            deploy_key = match key.get().await {
                Ok(key) => key,
                Err(err) => {
                    if err.kind == ErrorKind::NotFound {
                        create = true;
                    }
                    None
                }
            }
        }
        None => {
            let deploy_keys = _gitlab.list_deploy_keys(&key).await;
            match deploy_keys {
                Ok(keys) => {
                    for k in keys {
                        if k.same_id(&key) {
                            create = false;
                            deploy_key = Some(k);
                            break;
                        }
                    }
                }
                Err(_) => {
                    return;
                }
            }
        }
    }

    if create {
        _gitlab.create_deploy_key(&key).await.expect("msg");
    } else {
        //println!("deploy_key {:?}", deploy_key);
        if deploy_key.is_none() {
            return;
        }
        if deploy_key.clone().unwrap() == key {
            return;
        }
        println!("update key");
        if deploy_key.is_none() {
            return;
        }
        if deploy_key.clone().expect("msg") == key {
            return;
        }
        key.id = deploy_key.expect("msg").id;
        _gitlab.update_deploy_key(&key).await.expect("msg");
    }
    /*let deploy_keys = DeployKey::list(project).await;
    println!("keys are {:?}", deploy_keys);*/
}
